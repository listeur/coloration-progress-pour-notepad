Dans Notepad++ (version 6.2 minimum) :

- Aller dans le menu "Language"
- Cliquer sur "Define your language"
- Cliquer sur "importer" dans la fenetre qui s'ouvre
- Sélectionner le fichier xml fourni dans le zip :
    - coloration_progress_notepad_6.2.xml pour Notepad++ 6.2
    - coloration_progress_notepad_6.3.xml pour Notepad++ 6.3 et supérieur

Les fichiers .p .i .f et .pp seront automatiquement reconnu en tant que fichier Progress
Vous pouvez aussi choisir "Progress" dans le menu "Language" (tout en bas)

Bonne coloration !
